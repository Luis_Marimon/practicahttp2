
import java.util.HashMap;
import java.util.Map;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Luis
 */
public class HttpClientExampleTest {

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @org.junit.Test
    public void sendGet() throws Exception {
    }

    @org.junit.Test
    public void sendPost() throws Exception {
    }

    /**
     * Test of sendGet method, of class HttpClientExample.
     */
    @Test
    public void testSendGet() throws Exception {
        HttpClientExample httpClientExample = new HttpClientExample();

        //Rellenamos parametros que queremos enviar
        Map<String, String> parametros = new HashMap<String, String>();
        parametros.put("var1", "1");
        parametros.put("var2", "2");
        parametros.put("var3", "3");

        String param = httpClientExample.sendGet(parametros);

        System.out.println(param);

        String resul = "{  \"args\": {    \"var1\": \"1\",     \"var2\": \"2\",     \"var3\": \"3\"  }";
        boolean condicionJsonResultante = false;

        if (param.contains(resul)) {
            condicionJsonResultante = true;
        }

        //Comprovar que dentro del JSON resultante se encuentra:
        /* "args": {
                "var1": "1",
                "var2": "2",
                 "var3": "3"
                },
         */
        assertTrue(condicionJsonResultante);

    }

    @Test
    public void testSendPost() throws Exception {
        HttpClientExample httpClientExample = new HttpClientExample();

        //Rellenamos parametros que queremos enviar
        Map<String, String> mapParam = new HashMap<String, String>();
        mapParam.put("var1", "1");
        mapParam.put("var2", "2");
        mapParam.put("var3", "3");

        String param = httpClientExample.sendPost(mapParam);

        System.out.println(param);

        String res = "  \"form\": {    \"var1\": \"1\",     \"var2\": \"2\",     \"var3\": \"3\"  }";
        boolean condicionJsonResultante = false;

        if (param.contains(res)) {
            condicionJsonResultante = true;
        }
        assertTrue(condicionJsonResultante);
    }
}
