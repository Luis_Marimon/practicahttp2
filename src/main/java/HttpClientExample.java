
import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.http.client.HttpClient;
import static org.apache.http.HttpHeaders.USER_AGENT;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

/**
 * @author Luis
 */
public class HttpClientExample {

    // HTTP GET request
    public String sendGet(Map<String, String> variables) throws Exception {

        StringBuilder query = new StringBuilder();

        query.append("?");
        if (variables != null && variables.keySet().size() > 0) {
            for (String key : variables.keySet()) {
                query.append(key).append("=").append(variables.get(key)).append("&");
            }
        }
        String q = query.toString();
        String url = "http://httpbin.org/get" + q.substring(0, query.length() - 1);

        HttpClient client = new DefaultHttpClient();
        HttpGet request = new HttpGet(url);

        // add request header
        request.addHeader("User-Agent", USER_AGENT);

        HttpResponse response = client.execute(request);

        System.out.println("\nSending 'GET' request to URL : " + url);
        System.out.println("Response Code : "
                + response.getStatusLine().getStatusCode());

        BufferedReader rd = new BufferedReader(
                new InputStreamReader(response.getEntity().getContent()));

        StringBuffer res = new StringBuffer();
        String line = "";
        while ((line = rd.readLine()) != null) {
            res.append(line);
        }
        return res.toString();
    }
    // HTTP POST request

    public String sendPost(Map<String, String> parametros) throws Exception {

        /**
         * codigo del tutorial
         */
        List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();

        if (parametros != null && parametros.keySet().size() > 0) {
            for (String key : parametros.keySet()) {
                urlParameters.add(new BasicNameValuePair(key, parametros.get(key)));
            }
        }

        String url = "http://httpbin.org/post";

        HttpClient client = new DefaultHttpClient();
        HttpPost post = new HttpPost(url);

        // add header
        post.setHeader("User-Agent", USER_AGENT);

        post.setEntity(new UrlEncodedFormEntity(urlParameters));

        HttpResponse response = client.execute(post);
        System.out.println("\nSending 'POST' request to URL : " + url);
        System.out.println("Post parameters : " + post.getEntity());
        System.out.println("Response Code : "
                + response.getStatusLine().getStatusCode());

        BufferedReader rd = new BufferedReader(
                new InputStreamReader(response.getEntity().getContent()));

        StringBuffer res = new StringBuffer();
        String line = "";
        while ((line = rd.readLine()) != null) {
            res.append(line);
        }

        return res.toString();
    }
}
